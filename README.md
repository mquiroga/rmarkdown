# Rmarkdown for reproducible, publication-ready documents

Did you know that you can write your thesis, article, or even book, in a reproducible manner without having to suffer through complex formatting in Word or having to learn LaTeX? Or that you can prepare a report to update your collaborators on the latest plots and tables for an analysis without having to export your figure to png, save it somewhere, insert it into a document, resize, etc.? 

Here are some minimal examples and references to get you started and keep you going with writing robust and reproducible documents with Rmarkdown.

> This material was adapted from an [R-Ladies session on Rmarkdown](https://github.com/R-LadiesMelbourne/2019-02-27-publications-ready-documents-with-Rmarkdown) led by [Anna Quaglieri](https://github.com/annaquaglieri16) and [Soroor Zadeh](https://github.com/soroorh) on [27/02/2019](https://www.meetup.com/R-Ladies-Melbourne/events/258884903/)
