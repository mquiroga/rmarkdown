# Introduction
\label{sec:intro}

This is an introduction to the flexible `bookdown` [@bookdown] package. `bookdown` extends the syntax provided by Rmarkdown, allowing automatic numbering of figures, tables, equations, and cross-referencing within a single document.